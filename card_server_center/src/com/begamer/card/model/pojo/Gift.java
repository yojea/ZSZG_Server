package com.begamer.card.model.pojo;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName: Gift
 * @Description:礼包
 * @author gs
 * @date 2014-5-5 下午05:10:56
 * 
 */
public class Gift 
{
	private int id;
	private String name;//礼包名字
	private int gold;//金币
	private int crystal;//水晶
	private int runeNum;//符文值
	private int power;//体力
	private String card;//格式:cardId,cardId
	private String skill;//主动技能:skillId,skillId
	private String pSkill;//被动技能:pSkillId,pSkillId
	private String equip;//装备:equipId,equipId
	private String item;//材料:itemId,itemId
	private String startDate;// 开始时间
	private String endDate;// 结束时间
	private String updateDate;// 创建时间
	private int num;// 激活码数量
	private int finalNum;//激活码使用量 

	private ConcurrentHashMap<String, Key> keys = new ConcurrentHashMap<String, Key>();

	public Gift()
	{}
	
	public Gift(String name, int gold, int crystal, int runeNum,
			int power, String card, String skill, String pSkill, String equip,
			String item, String startDate, String endDate, String updateDate,
			int num)
	{
		this.name = name;
		this.gold = gold;
		this.crystal = crystal;
		this.runeNum = runeNum;
		this.power = power;
		this.card = card;
		this.skill = skill;
		this.pSkill = pSkill;
		this.equip = equip;
		this.item = item;
		this.startDate = startDate;
		this.endDate = endDate;
		this.updateDate = updateDate;
		this.num = num;
		this.finalNum = 0;
	}

	public Gift(int id, String name, int gold, int crystal, int runeNum,
			int power, String card, String skill, String pSkill, String equip,
			String item, String startDate, String endDate, String updateDate,
			int num,int finalNum)
	{
		super();
		this.id = id;
		this.name = name;
		this.gold = gold;
		this.crystal = crystal;
		this.runeNum = runeNum;
		this.power = power;
		this.card = card;
		this.skill = skill;
		this.pSkill = pSkill;
		this.equip = equip;
		this.item = item;
		this.startDate = startDate;
		this.endDate = endDate;
		this.updateDate = updateDate;
		this.num = num;
		this.finalNum = finalNum;
	}

	public void setKeys(List<Key> keys)
	{
		if(keys!=null && keys.size()>0)
		{
			for(Key key:keys)
			{
				this.keys.put(key.getCode(), key);
			}
		}
	}
	
	public Key getKey(String code)
	{
		return keys.get(code);
	}
	
	public boolean haveGot(int serverId,int playerId)
	{
		for(Key key:keys.values())
		{
			if(key.getStatus()==1 && key.getServerId()==serverId && key.getPlayerId()==playerId)
			{
				return true;
			}
		}
		return false;
	}
	
	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getGold()
	{
		return gold;
	}

	public void setGold(int gold)
	{
		this.gold = gold;
	}

	public int getCrystal()
	{
		return crystal;
	}

	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}

	public int getRuneNum()
	{
		return runeNum;
	}

	public void setRuneNum(int runeNum)
	{
		this.runeNum = runeNum;
	}

	public int getPower()
	{
		return power;
	}

	public void setPower(int power)
	{
		this.power = power;
	}

	public String getCard()
	{
		return card;
	}

	public void setCard(String card)
	{
		this.card = card;
	}

	public String getSkill()
	{
		return skill;
	}

	public void setSkill(String skill)
	{
		this.skill = skill;
	}

	public String getpSkill()
	{
		return pSkill;
	}

	public void setpSkill(String pSkill)
	{
		this.pSkill = pSkill;
	}

	public String getEquip()
	{
		return equip;
	}

	public void setEquip(String equip)
	{
		this.equip = equip;
	}

	public String getItem()
	{
		return item;
	}

	public void setItem(String item)
	{
		this.item = item;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public String getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(String updateDate)
	{
		this.updateDate = updateDate;
	}

	public int getNum()
	{
		return num;
	}

	public void setNum(int num)
	{
		this.num = num;
	}

	public int getFinalNum()
	{
		return finalNum;
	}

	public void setFinalNum(int finalNum)
	{
		this.finalNum = finalNum;
	}
	
}
