package com.begamer.card.json;

public class HaimaPayJson
{
	public String exorderno;//外部订单号，商户生成的订单号
	public String transid;//交易流水号，计费支付平台的交易流水号
	public String appid;//平台为商户应用分配的唯一id
	public int waresid;//商品编码 ，默认为1
	public int feetype;//计费方式
	public int money;//交易金额，单位是分
	public int count;//本次购买的商品数量
	public int result;//交易结果，0成功，1失败
	public int transtype;//交易类型，0交易，1冲正
	public String transtime;
	public String cpprivate;
	public int paytype;//支付方式
	
	public String getExorderno() {
		return exorderno;
	}
	public void setExorderno(String exorderno) {
		this.exorderno = exorderno;
	}
	public String getTransid() {
		return transid;
	}
	public void setTransid(String transid) {
		this.transid = transid;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}
	public int getWaresid() {
		return waresid;
	}
	public void setWaresid(int waresid) {
		this.waresid = waresid;
	}
	public int getFeetype() {
		return feetype;
	}
	public void setFeetype(int feetype) {
		this.feetype = feetype;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	public int getTranstype() {
		return transtype;
	}
	public void setTranstype(int transtype) {
		this.transtype = transtype;
	}
	public String getTranstime() {
		return transtime;
	}
	public void setTranstime(String transtime) {
		this.transtime = transtime;
	}
	public String getCpprivate() {
		return cpprivate;
	}
	public void setCpprivate(String cpprivate) {
		this.cpprivate = cpprivate;
	}
	public int getPaytype() {
		return paytype;
	}
	public void setPaytype(int paytype) {
		this.paytype = paytype;
	}
}
