package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SensitivewordsData implements PropertyReader {

	public int id;
	public String chinese;
	
	private static HashMap<Integer, SensitivewordsData> data =new HashMap<Integer, SensitivewordsData>();
	private static List<SensitivewordsData> dataList =new ArrayList<SensitivewordsData>();
	
	@Override
	public void addData() {
		data.put(id, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss) {

	}

	@Override
	public void resetData() {
		dataList.clear();
		data.clear();
	}

	public static SensitivewordsData getSensitivewordsData(int index)
	{
		return data.get(index);
	}
	
	public static List<SensitivewordsData> getSensitivewordsDatas()
	{
		return dataList;
	}
}
