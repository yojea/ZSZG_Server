package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class PowerData implements PropertyReader
{
	//==编号==//
	public int id;
	//==系数==//
	public float number;
	
	private static HashMap<Integer, PowerData> data=new HashMap<Integer, PowerData>();
	
	@Override
	public void addData()
	{
		data.put(id,this);
	}

	@Override
	public void parse(String[] ss)
	{
	}

	@Override
	public void resetData()
	{
		data.clear();
	}

	public static float getMul(int id)
	{
		return data.get(id).number;
	}
	
}
