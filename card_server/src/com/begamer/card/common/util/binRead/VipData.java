package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.begamer.card.common.Constant;

public class VipData implements PropertyReader
{
	/**vip等级**/
	public int level;
	/**ios金额**/
	public int costios;
	/**安卓金额**/
	public int costandroid;
	/**描述**/
	public String description;
	/**点金手**/
	public int gold;
	/**买体力**/
	public int energy;
	/**迷宫重置**/
	public int maze;
	/**普通次数**/
	public int normal;
	/**精英次数**/
	public int hero;
	/**活动副本次数**/
	public int activity;
	/**副本扫荡**/
	public int mission;
	/**多次扫荡**/
	public int missiontimes;
	/**初始怒气**/
	public int energystart;
	/**怒气上限**/
	public int maxenergy;
	/**pvp次数**/
	public int pvptime;
	/**自动强化**/
	public int autoupgrade;
	/**vip礼包id**/
	public int giftid;
	public int fakeprice;
	public int realprice;
	
	private static HashMap<Integer, VipData> data =new HashMap<Integer, VipData>();
	private static List<VipData> dataList=new ArrayList<VipData>();
	@Override
	public void addData()
	{
		data.put(level, this);
		dataList.add(this);
	}

	@Override
	public void parse(String[] ss)
	{
		
	}

	@Override
	public void resetData()
	{
		dataList.clear();
		data.clear();
	}
	
	/**根据等级获取一个data**/
	public static VipData getVipData(int lv)
	{
		return data.get(lv);
	}
	
	public static Integer getDataSize()
	{
		return data.size();
	}
 
	public static int getVipLevel(int cost,String platform)
	{
		for(int k=dataList.size()-1;k>=0;k--)
		{
			VipData vd=dataList.get(k);
			if(Constant.OS_ANDROID.equals(platform) && cost>=vd.costandroid)
			{
				return vd.level;
			}
			if(Constant.OS_IOS.equals(platform) && cost>=vd.costios)
			{
				return vd.level;
			}
			if (Constant.OS_PC.equals(platform) && cost>=vd.costandroid)
			{
				return vd.level;
			}
		}
		return 0;
	}
	
	public static int getInitEnergy(int vipLevel)
	{
		VipData vd=VipData.getVipData(vipLevel);
		if(vd!=null)
		{
			return vd.energystart;
		}
		return 0;
	}
}
