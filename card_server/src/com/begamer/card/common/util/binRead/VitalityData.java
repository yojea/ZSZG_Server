package com.begamer.card.common.util.binRead;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.begamer.card.common.util.StringUtil;
import com.begamer.card.log.ErrorLogger;

public class VitalityData implements PropertyReader {

	private static final HashMap<Integer, VitalityData> data = new HashMap<Integer, VitalityData>();

	public int vitality;
	public String icon;
	public List<String> reward;
	private static final Logger errorlogger = ErrorLogger.logger;

	@Override
	public void addData() {
		data.put(vitality, this);
	}

	@Override
	public void parse(String[] ss) {
		int location = 0;
		vitality = StringUtil.getInt(ss[location]);
		icon = StringUtil.getString(ss[location + 1]);
		reward = new ArrayList<String>();
		for (int i = 0; i < 7; i++) {
			location = 2 + i * 2;
			int rewardType = StringUtil.getInt(ss[location]);
			String rewardid = StringUtil.getString(ss[location + 1]);
			String str = rewardType + "-" + rewardid;
			try {
				if (rewardType == 0) {
					continue;
				} else {
					reward.add(str);
				}
			} catch (Exception e) {
				errorlogger.error("加载vitality奖励数据格式错误:" + vitality, e);
				System.exit(0);
			}
		}
		addData();
	}

	
	public static VitalityData getData(int active){
		return data.get(active);
	}
	
	public static HashMap<Integer, VitalityData> getAllData(){
		return data;
	}
	
	@Override
	public void resetData() {
		data.clear();
	}

}
