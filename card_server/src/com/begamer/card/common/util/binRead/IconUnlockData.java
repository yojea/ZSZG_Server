package com.begamer.card.common.util.binRead;

import java.util.HashMap;

public class IconUnlockData implements PropertyReader
{
	public int ID;
	public int unlock;
	public String headIcon1;
	public String headIcon2;
	public String headIcon3;
	
	private static HashMap<Integer, IconUnlockData> data =new HashMap<Integer, IconUnlockData>();
	@Override
	public void addData()
	{
		data.put(ID, this);
	}

	@Override
	public void parse(String[] ss)
	{

	}

	@Override
	public void resetData()
	{
		data.clear();
	}
	
	/**根据id获取data**/
	public IconUnlockData getIconUnlockData(int index)
	{
		return data.get(index);
	}
	
	/**根据战力筛选data**/
	public static Integer getIconUnlockDataByUnlock(int unlock ,int iconUnlock)
	{
		int k=0;
		for(IconUnlockData iud : data.values())
		{
			if(iud.unlock<unlock)
			{
				if(iud.unlock>iconUnlock)
				{
					k =iud.ID;
				}
			}
		}
		if(k==0)
		{
			k =iconUnlock;
		}
		return k;
	}
}
