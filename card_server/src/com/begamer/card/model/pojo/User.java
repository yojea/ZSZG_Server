package com.begamer.card.model.pojo;

/**
 * 
 * @ClassName: User
 * @Description: TODO
 * @author gs
 * @date Nov 1, 2011 2:23:02 PM
 * 
 */
public class User {
	private int id;
	private int serverId;
	private String name;
	private String password;
	private String nickname;
	private String platform;
	private String email;
	private String createOn;
	private String updateOn;
	private String ip;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getCreateOn()
	{
		return createOn;
	}

	public void setCreateOn(String createOn)
	{
		this.createOn = createOn;
	}

	public String getUpdateOn()
	{
		return updateOn;
	}

	public void setUpdateOn(String updateOn)
	{
		this.updateOn = updateOn;
	}

	public int getServerId()
	{
		return serverId;
	}

	public void setServerId(int serverId)
	{
		this.serverId = serverId;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public String getIp()
	{
		return ip;
	}

	public String getNickname()
	{
		return nickname;
	}

	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}

	public String getPlatform()
	{
		return platform;
	}

	public void setPlatform(String platform)
	{
		this.platform = platform;
	}

}
