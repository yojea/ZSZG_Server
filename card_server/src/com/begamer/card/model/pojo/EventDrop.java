package com.begamer.card.model.pojo;


public class EventDrop {

	private int id;
	private int eventType;//关卡类型(1.推图关卡,2.普通关卡,3.精英关卡,4.迷宫战斗,5.全部)
	private String startDay;
	private String endDay;
	private String reward1;//格式type&id&number&pro type:1item  2equip  3card  4skill  5pskill
	private String reward2;
	private String reward3;
	private String reward4;
	private String reward5;
	private String reward6;
	private int sell;//1可用,0不可用
	
	public static EventDrop creatEventDrop(int eventType,String startDay,String endDay,String reward1,String reward2,String reward3,String reward4,String reward5,String reward6)
	{
		EventDrop eventDrop = new EventDrop();
		eventDrop.setEventType(eventType);
		eventDrop.setStartDay(startDay);
		eventDrop.setEndDay(endDay);
		eventDrop.setReward1(reward1);
		eventDrop.setReward2(reward2);
		eventDrop.setReward3(reward3);
		eventDrop.setReward4(reward4);
		eventDrop.setReward5(reward5);
		eventDrop.setReward6(reward6);
		eventDrop.setSell(1);
		return eventDrop;
	}
	
	/**获取奖励**/
	public String[] getRewards()
	{
		return new String[]{reward1,reward2,reward3,reward4,reward5,reward6};
	}
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getEventType()
	{
		return eventType;
	}
	public void setEventType(int eventType)
	{
		this.eventType = eventType;
	}

	public String getStartDay()
	{
		return startDay;
	}

	public void setStartDay(String startDay)
	{
		this.startDay = startDay;
	}

	public String getEndDay()
	{
		return endDay;
	}

	public void setEndDay(String endDay)
	{
		this.endDay = endDay;
	}

	public String getReward1()
	{
		return reward1;
	}

	public void setReward1(String reward1)
	{
		this.reward1 = reward1;
	}

	public String getReward2()
	{
		return reward2;
	}

	public void setReward2(String reward2)
	{
		this.reward2 = reward2;
	}

	public String getReward3()
	{
		return reward3;
	}

	public void setReward3(String reward3)
	{
		this.reward3 = reward3;
	}

	public String getReward4()
	{
		return reward4;
	}

	public void setReward4(String reward4)
	{
		this.reward4 = reward4;
	}

	public String getReward5()
	{
		return reward5;
	}

	public void setReward5(String reward5)
	{
		this.reward5 = reward5;
	}

	public String getReward6()
	{
		return reward6;
	}

	public void setReward6(String reward6)
	{
		this.reward6 = reward6;
	}

	public int getSell()
	{
		return sell;
	}

	public void setSell(int sell)
	{
		this.sell = sell;
	}

	
}
