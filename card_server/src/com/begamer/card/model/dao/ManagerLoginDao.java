package com.begamer.card.model.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.begamer.card.model.pojo.Manager;

public class ManagerLoginDao extends HibernateDaoSupport {
	
	private Logger logger = Logger.getLogger(ManagerLoginDao.class);
	
	public Manager ManagerLogin(String name, String password)
	{
		
		Session session = getSession();
		
		try
		{
			String hql = "from Manager m where m.name=? and m.password=?";
			Query query = session.createQuery(hql);
			query.setString(0, name);
			query.setString(1, password);
			Manager loginJson = (Manager) query.uniqueResult();
			return loginJson;
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	
	public void addManager(Manager manager)
	{
		
		Session session = getSession();
		Transaction ts = session.beginTransaction();
		try
		{
			session.save(manager);
			ts.commit();
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	
	public void upManager(Manager manager)
	{
		
		Session session = getSession();
		Transaction ts = session.beginTransaction();
		try
		{
			String hql = "update Manager m set m.name=?,m.password=? where m.id=?";
			Query query = session.createQuery(hql);
			query.setString(0, manager.getName());
			query.setString(1, manager.getPassword());
			query.setInteger(2, manager.getId());
			query.executeUpdate();
			ts.commit();
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Manager> getManagers()
	{
		
		Session session = getSession();
		try
		{
			String hql = "from Manager m";
			Query query = session.createQuery(hql);
			return query.list();
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	
	public Manager getOneById(int id)
	{
		
		Session session = getSession();
		try
		{
			Query query = session.createQuery("from Manager m where m.id=?");
			query.setInteger(0, id);
			Manager manager = (Manager) query.uniqueResult();
			return manager;
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
		
	}
	
	public Manager getManagerByName(String name)
	{
		
		Session session = getSession();
		try
		{
			Query query = session.createQuery("from Manager m where m.name=?");
			query.setString(0, name);
			Manager manager = (Manager) query.uniqueResult();
			return manager;
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	
	/***************************************************************************
	 * 模糊查询：根据名称
	 * 
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Manager> mh_name(String name)
	{
		Session session = getSession();
		try
		{
			Query query = session.createQuery("from Manager m where m.name like '%" + name + "%'");
			List<Manager> managers = query.list();
			return managers;
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			
			session.close();
		}
	}
	
	/** 根据区服查询GM* */
	@SuppressWarnings("unchecked")
	public List<Manager> getManagerByServerId(int ServerId)
	{
		Session session = getSession();
		try
		{
			Query query = session.createQuery("from Manager m where m.serverId=?");
			query.setInteger(0, ServerId);
			List<Manager> list = query.list();
			return list;
		}
		catch (RuntimeException e)
		{
			throw e;
		}
		finally
		{
			session.close();
		}
	}
	
	/** 删除 **/
	public void delManager(Manager manager)
	{
		Session session = getSession();
		try
		{
			Query query = session.createQuery("delete from Privilege p where p.masterValue=?");
			query.setInteger(0, manager.getId());
			query.executeUpdate();
			getHibernateTemplate().delete(manager);
		}
		catch (Exception e)
		{
			logger.debug("del Manager failed" + e);
		}
	}
}
