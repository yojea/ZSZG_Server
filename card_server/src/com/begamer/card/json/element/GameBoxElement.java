package com.begamer.card.json.element;

public class GameBoxElement
{
	public int goodsType;//物品类型
	public int goodsId;//物品id
	public int num;//数量
	public int dropType;//掉落类型
	public int getGoodsType() {
		return goodsType;
	}
	public void setGoodsType(int goodsType) {
		this.goodsType = goodsType;
	}
	public int getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(int goodsId) {
		this.goodsId = goodsId;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public int getDropType() {
		return dropType;
	}
	public void setDropType(int dropType) {
		this.dropType = dropType;
	}
	
	
}
