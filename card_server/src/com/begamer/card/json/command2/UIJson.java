package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class UIJson extends BasicJson
{
	/**
	 * 0 玩家信息
	 * ui编号
	 * 1：背包
	 * 2：背包中可出售的物品
	 * 3：主界面
	 * 4:卡组
	 * 5:获取一个类型的所有数据
	 * 6:设置战斗卡组
	 * 7:吞噬者数据 
	 * 8：被吞噬着数据
	 * 9
	 * 12:合成，已经解锁的合成物品
	 * 14:合成，合成某物品所需的材料
	 * 18:已经解锁迷宫列表
	 * 19：进入迷宫
	 * 20:迷宫，每个格子掉落的物品
	 * 21：进入冥想界面
	 * 22：自动回复体力
	 * 23：地图选择
	 * 24:活动副本主界面
	 * 25:副本选择关卡界面
	 * 26:查看成就列表
	 * 27：迷宫付费重置
	 * 28:抽卡
	 * 29：签到
	 * 30:突破-可以突破的卡
	 * 31：突破-可以被突破
	 * 32：请求解锁模块
	 * 33:邮箱
	 * 34:头像区域设置
	 * 35:头像修改
	 * 36:军团名称修改
	 * 37:掉落指引
	 * 38:卡牌详细信息，是否有新装备提醒或者提升
	 * 39:vip充值主界面
	 * 
	 * 41在线礼包领取
	 * 42:ko兑换界面1
	 * 43:ko兑换界面2
	 * 44ko兑换功能
	 * 45商城界面(type 1商城 2黑市 3黑市刷新)
	 * 46背包全选设定
	 * 47七天登陆奖励
	 * 48七天有礼领取奖励
	 * 49聚宝盆界面
	 * 50聚宝按钮，开始聚宝
	 * 51请求是否有新解锁的合体技
	 * 52请求等级奖励界面
	 * 53领取等级奖励
	 * 54迷宫血瓶使用
	 * 55竞技场刷新对手玩家
	 * 56迷宫许愿
	 * 57迷宫通关大奖
	 * 58转盘档位界面
	 * 59转盘详细界面
	 * 60领取转动奖励
	 * 61抽卡排行活动界面
	 */
	public int ui;
	
	/**ui为1时背包类型:1角色卡,2主动技能和被动技能,3装备,4材料**/
	/**ui为4时表示卡组索引---0~4**/
	/**ui为7,8时背包类型:1角色卡,2主动技能,3被动技能,4装备,5材料**/
	/**合成时2为equip,1card,3item**/
	/**活动副本id**/
	public int type;

	/**第几组****ui=8:吞噬者索引**/
	/**合成材料时,i为合成物品id**/
	/**迷宫时表示迷宫编号**/
	public int i;
	/**强化时显示，0为全部显示，1为同族显示**/
	/**迷宫掉落物品时表示进度**/
	public int show;

	public String str;
	public int getUi()
	{
		return ui;
	}

	public void setUi(int ui)
	{
		this.ui = ui;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getI()
	{
		return i;
	}

	public void setI(int i)
	{
		this.i = i;
	}
	
		public int getShow() {
		return show;
	}

	public void setShow(int show) {
		this.show = show;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
}