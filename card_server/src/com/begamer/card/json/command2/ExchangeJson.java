package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class ExchangeJson extends BasicJson {

	public int id;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}
}
