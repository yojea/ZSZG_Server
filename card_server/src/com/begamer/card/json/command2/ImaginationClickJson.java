package com.begamer.card.json.command2;

import com.begamer.card.json.BasicJson;

public class ImaginationClickJson extends BasicJson {
	/**点击的NPC  id**/
	public int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
