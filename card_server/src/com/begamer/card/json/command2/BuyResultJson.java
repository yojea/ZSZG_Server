package com.begamer.card.json.command2;

import com.begamer.card.json.ErrorJson;

public class BuyResultJson extends ErrorJson
{
	public int buyTimes;
	public int gold;
	public int crystal;
	public int bagSizePs;//被动技能的剩余空间
	
	public int getBuyTimes()
	{
		return buyTimes;
	}

	public void setBuyTimes(int buyTimes)
	{
		this.buyTimes = buyTimes;
	}

	public int getGold()
	{
		return gold;
	}

	public void setGold(int gold)
	{
		this.gold = gold;
	}

	public int getCrystal()
	{
		return crystal;
	}

	public void setCrystal(int crystal)
	{
		this.crystal = crystal;
	}

	public int getBagSizePs() {
		return bagSizePs;
	}

	public void setBagSizePs(int bagSizePs) {
		this.bagSizePs = bagSizePs;
	}
	
}
