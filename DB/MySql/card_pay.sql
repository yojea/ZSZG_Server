/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50610
Source Host           : localhost:3306
Source Database       : card_pay

Target Server Type    : MYSQL
Target Server Version : 50610
File Encoding         : 65001

Date: 2016-02-19 10:00:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gm_manager
-- ----------------------------
DROP TABLE IF EXISTS `gm_manager`;
CREATE TABLE `gm_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serverId` int(11) DEFAULT '0' COMMENT '服务器ID',
  `name` varchar(20) DEFAULT '' COMMENT '用户名',
  `password` varchar(20) DEFAULT '' COMMENT '密码',
  `playerId` int(11) DEFAULT '0' COMMENT '游戏账号',
  `createon` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '' COMMENT '创建日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gm_manager
-- ----------------------------
INSERT INTO `gm_manager` VALUES ('1', '2', 'admin', '123456', '2', '2014-04-14 13:33:58');
INSERT INTO `gm_manager` VALUES ('9', '2', 'shengyou', '123456', '0', '');

-- ----------------------------
-- Table structure for payinfo
-- ----------------------------
DROP TABLE IF EXISTS `payinfo`;
CREATE TABLE `payinfo` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERID` varchar(128) DEFAULT NULL COMMENT '用户的通行证账号',
  `USERNUMID` varchar(128) DEFAULT NULL COMMENT '用户通行证数字ID',
  `GAMEID` varchar(128) DEFAULT NULL COMMENT '游戏id',
  `REQTIME` bigint(64) DEFAULT '0' COMMENT '用户消费时间',
  `STATE` varchar(10) DEFAULT NULL COMMENT '消费状态：1 成功；0 失败',
  `CONSUMEVALUE` varchar(50) DEFAULT NULL COMMENT '单位元',
  `EXTRADATA` varchar(500) DEFAULT NULL COMMENT '请求方对此不作处理，可以为空',
  `GAMESERVERZONE` varchar(50) DEFAULT NULL COMMENT '用户此次消费所在游戏服务器分区',
  `CONSUMEID` varchar(128) DEFAULT NULL COMMENT '消费订单号',
  `REQDATE` varchar(128) DEFAULT NULL,
  `GAMEORDERID` varchar(128) DEFAULT NULL COMMENT '游戏订单Id',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of payinfo
-- ----------------------------

-- ----------------------------
-- Table structure for pay_order
-- ----------------------------
DROP TABLE IF EXISTS `pay_order`;
CREATE TABLE `pay_order` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `CONSUMVALUE` varchar(128) DEFAULT NULL,
  `EXTRA` varchar(500) DEFAULT NULL,
  `SERVERID` varchar(128) DEFAULT NULL,
  `PLAYERID` varchar(128) DEFAULT NULL,
  `USERID` varchar(128) DEFAULT NULL,
  `REQTIME` varchar(128) DEFAULT NULL,
  `STATUS` int(11) DEFAULT '0' COMMENT '0未支付，1已支付',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pay_order
-- ----------------------------
